# ICU Alarm Management

## How to start developing

In order to start developing you should enter the python environment of your choice and run

```
make requirements
```

## Testing
In order to test your Code use
```
make test
```


## Structure of the repository
Alarm management for intensive care units.
```
.
├── Makefile
├── README.md
├── data                                <- Raw Data for development
│   └── csv_sample.csv
├── icu_alarm                           <- Source Code of the package
│   ├── CSVDataSource.py
│   ├── __init__.py
│   ├── csv_data_source.py
│   ├── data_sink_base_class.py
│   ├── data_source_base_class.py
│   ├── drift_model.py
│   ├── icu_data_interface.py
│   ├── model_base_class.py
│   └── test                           <- Directory for your tests
│       ├── __init__.py
│       └── test_drift_model.py
├── requirements.txt
├── setup.py
```