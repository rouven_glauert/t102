from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Alarm management for intensive care units.',
    author='idalab GmbH',
    license='MIT',
)
