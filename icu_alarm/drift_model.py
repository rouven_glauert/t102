class DriftModel():
    """A model computing the derivation from the normal vital signs of a patient.

    The model provides an way to compute the distance to the optimal
    values of vital signs.
    The optimal values are defined as:
        body_temperature: 36.5
        blood_pressure_systolic: 124
        blood_pressure_diastolic: 82
        heart_rate: 70
        respiratory_rate: 14.5
    """
    def score(self, body_temperature: float, blood_pressure_systolic: float,
              blood_pressure_diastolic: float, heart_rate: float,
              respiratory_rate: float):
        """Computes the average percentual deviation from the optimal
        vital sign values.

        Args:
            body_temperature (float): body temprature of the patient
            blood_pressure_systolic (float): systolic blood pressure of
                                             the patient
            blood_pressure_diastolic (float): diastolic blood
                                              pressure of the patient
            heart_rate (float): heart rate of the patient
            respiratory_rate (float): respiratory rate of the patient

        Returns:
            float: score between 0 and infinity to score the
                   deviation of the vital signs.
        """

        deviation = [
            abs(self.body_temperature - body_temperature) / body_temperature,
            abs(self.blood_pressure_systolic - blood_pressure_systolic) /
            blood_pressure_systolic,
            abs(self.blood_pressure_diastolic - blood_pressure_diastolic) /
            blood_pressure_diastolic,
            abs(self.heart_rate - heart_rate) / heart_rate,
            abs(self.respiratory_rate - respiratory_rate) / respiratory_rate
        ]

        return sum(deviation) / 5

    @property
    def body_temperature(self):
        return 36.5

    @property
    def blood_pressure_systolic(self):
        return 124

    @property
    def blood_pressure_diastolic(self):
        return 82

    @property
    def heart_rate(self):
        return 70

    @property
    def respiratory_rate(self):
        return 14.5
