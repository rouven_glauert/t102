class DataSource:
    """This class delivers the data to the model.

    The class manages the access to the data.
    It provides the python interface for iterators,
    so that it can be called with a for ... in ...: loop.

    """
    def __init__(self) -> None:
        pass

    def __iter__(self):
        raise NotImplementedError

    def __next__(self):
        raise NotImplementedError
