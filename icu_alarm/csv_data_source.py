from icu_alarm.data_source_base_class import DataSource


class CSVDataSource(DataSource):
    """Handles the data input from a CSV file

    This class handles the loading of a CSV file.
    It inherits from the DataSource class and thus already
    implements the necessary featues.

    Args:
        CSVDataSource ([type]): [description]
    """
