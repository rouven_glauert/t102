import unittest
from icu_alarm.drift_model import DriftModel


class TestDriftModel(unittest.TestCase):
    def test_score(self):
        # Assign
        model = DriftModel()
        sample_input = [36.47, 123.58, 82.75, 67.25, 15.16]
        expectation = 0.0195

        # Act
        reality = model.score(*sample_input)

        # Assert
        self.assertAlmostEqual(expectation, reality, places=3)
