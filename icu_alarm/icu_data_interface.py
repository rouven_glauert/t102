import requests
import json


class ICUDataInterface:
    """Class that serves as a python interface for the WebAPI
    """

    url = "https://idalab-icu.ew.r.appspot.com/patients/vital_signs"

    def get_data(self):
        """Returns a list of patient vital signs

        Accesses the web API for new data and return the data
        as a list.

        Returns:
            List: List of patient vital
        """
        data = requests.get(self.url)
        parsed = json.loads(data.text)
        return parsed
